<!-- markdownlint-disable-next-line MD041 -->
## hardware/userspace/ipxe

IPXE userspace package test

- enabled: `true`
- provision:
  - how: virtual
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /ipxe/iso

## hardware/userspace/redfish-finder

redfish-finder userspace package test

- enabled: `true`
- provision:
  - how: virtual
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /redfish/redfish-finder
