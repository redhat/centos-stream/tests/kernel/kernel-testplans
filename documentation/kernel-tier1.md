<!-- markdownlint-disable-next-line MD041 -->
## kernel-tier1/baremetal

Upstream kernel tier1 tests

- enabled: `true`
- provision:
  - how: virtual
  - hardware:
`{
    "virtualization": {
        "is-virtualized": false
    }
}`
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests>
  - filter: `tag: KernelTier1`
  - test:
    - /distribution/ltp/lite

## kernel-tier1/no-hw

Upstream kernel tier1 tests

- enabled: `true`
- provision:
  - how: virtual
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests>
  - filter: `tag: KernelTier1`
  - exclude:
    - /distribution/ltp/lite

## kernel-tier1/vm

Upstream kernel tier1 tests

- enabled: `true`
- provision:
  - how: virtual
  - hardware:
`{
    "virtualization": {
        "is-virtualized": true
    }
}`
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests>
  - filter: `tag: KernelTier1`
  - test:
    - /distribution/ltp/lite
