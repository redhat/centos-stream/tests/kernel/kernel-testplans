<!-- markdownlint-disable-next-line MD041 -->
## storage/iscsi/offload/be2iscsi

iSCSI tests using interfaces using be2iscsi driver

- enabled: `true`
- provision:
  - how: beaker
  - image: `RHEL-9%`
  - hardware:
`{
    "hostname": "storageqe-84.lab.eng.brq2.redhat.com"
}`
- discover:
  - filter: `tag:iscsi-offload`

## storage/iscsi/offload/bnx2i

iSCSI tests using interfaces using bnx2i driver

- enabled: `true`
- provision:
  - how: beaker
  - image: `RHEL-9%`
  - hardware:
`{
    "hostname": "storageqe-83.lab.eng.brq2.redhat.com"
}`
- discover:
  - filter: `tag:iscsi-offload`

## storage/iscsi/offload/cxgb4i

iSCSI tests using interfaces using cxgb4i driver

- enabled: `true`
- provision:
  - how: beaker
  - image: `RHEL-9%`
  - hardware:
`{
    "hostname": "storageqe-87.lab.eng.brq2.redhat.com"
}`
- discover:
  - filter: `tag:iscsi-offload`

## storage/iscsi/offload/intel

iSCSI tests running on Intel adapter

- enabled: `true`
- provision:
  - how: beaker
  - image: `RHEL-9%`
  - hardware:
`{
    "hostname": "storageqe-82.lab.eng.brq2.redhat.com"
}`
- discover:
  - filter: `tag:iscsi-offload`

## storage/iscsi/offload/qedi

iSCSI tests using interfaces using qedi driver

- enabled: `true`
- provision:
  - how: beaker
  - image: `RHEL-9%`
  - hardware:
`{
    "hostname": "storageqe-86.lab.eng.brq2.redhat.com"
}`
- discover:
  - filter: `tag:iscsi-offload`
