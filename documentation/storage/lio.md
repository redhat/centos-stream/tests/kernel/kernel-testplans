<!-- markdownlint-disable-next-line MD041 -->
## storage/lio/core

Tier 0/1 LIO/targetcli tests

- enabled: `true`
- provision:
  - how: virtual
- discover:
  - filter: `['component:targetcli', 'tag:local', 'tag:-libiscsi', 'tier:0,1']`

## storage/lio/full

LIO/targetcli full test suite

- enabled: `true`
- provision:
  - how: virtual
- discover:
  - filter: `component:targetcli`
