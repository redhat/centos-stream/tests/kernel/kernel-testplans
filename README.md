# kernel-testplans

=======
Contains the test plans for kernel tests that are defined using fmf.

## list the tests in the plans

```bash
tmt run discover -v plan --name kernel-tier1
```
